var $ = function(qry) {
    if(qry.indexOf("#") === 0){
        return document.querySelector(qry);
    }
    return document.querySelectorAll(qry);
};

HTMLElement.prototype.$ = function(qry) {
    if(qry.indexOf("#") === 0){
        return document.querySelector(qry);
    }
    return this.querySelectorAll(qry);
};

let hiding = 0;
let loading = true;

class DD {

    /**
     * @type {Object|null} the logged in user if there is one. Null otherwise
     */
    static user = null;

    /**
     * empty constructor
     */
    constructor(){

    }

    /**
     * callback function for a successful login
     *
     * @callback loginSuccessCallback
     */

    /**
     * callback function for a failed login
     *
     * @callback loginFailedCallback
     */

    /**
     * login a user
     * @param {String} username
     * @param {String} password
     * @param {loginSuccessCallback} loginSuccess
     * @param {loginFailedCallback} loginFailed
     */
    static login(username, password, loginSuccess, loginFailed){
        DD.call({
            uri: 'sessions',
            data: {
                username: username,
                password: password
            },
            method: 'POST',
            success: function (response) {
                if('message' in response){
                    loginFailed();
                } else {
                    DD.user = response;
                    loginSuccess();
                }
            },
            failed: loginFailed
        });
    }

    /**
     * Callback when the user is logged in
     *
     * @callback isLoggedInSuccessCallback
     * @param {Object} the logged in user
     */

    /**
     * Callback when there is no logged in user
     *
     * @callback isLoggedInFailedCallback
     */

    /**
     * Check if a user is logged in
     * @param {isLoggedInSuccessCallback} callbackLoggedIn
     * @param {isLoggedInFailedCallback} callbackNotLoggedIn
     */
    static isLoggedIn(callbackLoggedIn, callbackNotLoggedIn){
        DD.call({
            uri: 'sessions',
            method: 'GET',
            success: function (response) {
                if(response.message){
                    callbackNotLoggedIn();
                } else {
                    DD.user = response;
                    callbackLoggedIn();
                }
            },
            failed: callbackNotLoggedIn
        });
    }

    /**
     * Callback for logout
     *
     * @callback logoutCallback
     */

    /**
     * Logout the current user
     * @param {logoutCallback} callback
     */
    static logout(callback){
        DD.call({
            uri: 'sessions',
            method: 'DELETE',
            success: function () {
                DD.deleteCookie('auth');
                DD.user = null;
                if(callback) {
                    callback();
                } else {
                    DD.showPage('login');
                }
            },
            failed: function () {
                alert('login failed');
            }
        });
    }

    /**
     * Callback for call registerSuccess
     *
     * @callback registerSuccessCallback
     * @param {Object} object with server response
     */

    /**
     * Callback for call registerFailed
     *
     * @callback registerFailedCallback
     */

    /**
     * Register a new user
     * @param {String} username
     * @param {String} password
     * @param {registerSuccessCallback} successCallback
     * @param {registerFailedCallback} failedCallback
     */
    static register(username, password, successCallback, failedCallback){
        DD.call({
            uri: 'users',
            method: 'POST',
            success: function (response) {
                DD.user = response;
                successCallback(response);
            },
            failed: failedCallback,
            data: {
                username: username,
                password: password
            }
        });
    }

    /**
     * Load the profile in the HTML if the user is logged in
     */
    static loadProfile(){
        if(DD.user) {
            $('#profile').innerHTML = "<a onclick='DD.logout();'>logout</a>";
        } else {
            $('#profile').innerHTML = "";
        }
    }

    /**
     * callback function for getPage
     *
     * @callback getPageCallback
     * @param {String} response with the page as an HTML string
     */

    /**
     * Retrieve a page from the server
     * @param {String} page - the name of the page
     * @param {getPageCallback} onload - callback function to execute when the page is loaded.
     */
    static getPage(page, onload){
        let p = page;
        if(page.indexOf(".html") === -1){
            p += ".html";
        }
        DD.call({
           uri: p,
           method: 'GET',
           success: onload,
           page: true
        });
    }

    /**
     * callback for a success call
     *
     * @callback callSuccessCallback
     * @param {Object|String} HTML string of object with response data
     */

    /**
     * callback for a failed call
     *
     * @callback callFailedCallback
     */

    /**
     * Create a server call
     * @param {String}   requestObject.uri - What uri to request
     * @param {String}   requestObject.method - The request method. Defaults to GET
     * @param {callSuccessCallback} requestObject.success - The function to call when the call returns a status of 2** with a response object
     * @param {callFailedCallback} requestObject.failed - The function to call when the call returns a status other then 2**
     * @param {Object}   requestObject.params - parameters to pass to the server in the uri
     * @param {Object}   requestObject.data - parameters to pass to the server in the body
     * @param {Boolean}  requestObject.page - Optional parameter if a page is requested
     */
    static call(requestObject){
        if(!requestObject.method) requestObject.method = "GET";
        if(!requestObject.success) requestObject.success = function () {};
        if(!requestObject.failed) requestObject.failed = function () {};
        if(!requestObject.params) requestObject.params = {};
        if(!requestObject.data) requestObject.data = {};
        let request = new XMLHttpRequest();

        let url = (requestObject.page ? 'pages/' : 'api/') + requestObject.uri;
        for (let param in requestObject.params) {
            url += (url.indexOf("?") < 0 ? "?" : "&") + param + "=" + requestObject[param];
        }

        request.open(requestObject.method.toUpperCase(), url, true);

        let auth = DD.getCookie('auth');
        if(auth){
            request.setRequestHeader("auth", auth);
        }

        request.onload = function() {
            if(!requestObject.page) DD.setCookie('auth', request.getResponseHeader("auth"), 60);
            if (request.status >= 200 && request.status < 300) {
                let response = {};
                try {
                    if(requestObject.page){
                        response = request.response;
                    } else {
                        response = JSON.parse(request.response);
                    }
                } catch (e) {
                    console.warn(request.response);
                    return;
                }
                requestObject.success(response);
            } else {
                console.error(request.response);
                requestObject.failed();
            }
        };

        request.onerror = function() {
            console.error("could not load " + requestObject.method + ": " + requestObject.uri);
            requestObject.failed();
        };
        request.send(JSON.stringify(requestObject.data));
    }

    /**
     * Save a cookie
     * @param {String} name - the name of the cookie
     * @param {String} value - The content of the cookie
     * @param {int} exMin - The expiration period in minutes
     */
    static setCookie(name, value, exMin) {
        let d = new Date();
        d.setTime(d.getTime() + (exMin*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = name + "=" + value + ";" + expires + ";path=/";
    }

    /**
     * Get the contents of a cookie
     * @param {String} name - the cookie name
     * @returns {string} the contents of the cookie
     */
    static getCookie(name) {
        name += "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /**
     * Delete a cookie from the client
     * @param {String} name - the name of the cookie
     */
    static deleteCookie(name){
        DD.setCookie(name, '', -1);
    }

    /**
     * Hide all currently shown pages
     */
    static hidePages() {
        let pages = $('.page');
        for(let i = 0; i < pages.length; i++) {
            let page = pages[i];
            if(page.classList.contains('show')) {
                page.classList.replace('show', 'hide');
                hiding = (new Date()).getTime();
            }
        }
        loading = true;
        setTimeout(function () {
            if(loading) {
                $('#loading').style.display = 'inline-block';
            }
        }, 600);
    }

    /**
     * Retrieve a page from the server and display it
     * @param {String} page - page name
     */
    static showPage(page){
        DD.hidePages();
        DD.getPage(page, function (response) {
            DD.loadProfile();
            setTimeout(function () {
                let div = $('.page')[0];
                div.id = 'page_' + page;
                div.innerHTML = response;
                if(div.$('script').length > 0){
                    eval(div.$('script')[0].innerHTML);
                }
                loading = false;
                $('#loading').style.display = 'none';
                let p  = $('#page_' + page);
                p.classList.remove('hide');
                p.classList.add('show');
                setTimeout(function () {
                    let inp = p.getElementsByTagName('input');
                    if(inp.length > 0){
                        inp[0].focus();
                    }
                }, 10);
            }, Math.max(hiding + 350 - (new Date()).getTime(), 0));
        });

    }

    /**
     * create a popup screen
     * @param {String} html - Content of the popup
     * @param {int} x - the start x position
     * @param {int} y - the start y position
     */
    static popup(html, x, y){
        let modal = document.createElement('div');
        modal.className = 'modal';
        modal.addEventListener('click', function (e) {
            modal.remove();
        });

        let left = x / document.body.clientWidth * 100;
        let top = y / document.body.clientHeight * 100;

        let popup = document.createElement('div');
        popup.style.left = left + '%';
        popup.style.top = top + '%';
        popup.innerHTML = html;
        popup.addEventListener('click', function (e) {
            e.stopPropagation();
        });
        modal.appendChild(popup);

        document.body.appendChild(modal);
    }

    /**
     * A warning for the user
     * @param {String} text - Content of the warning
     */
    static warning(text){
        DD.showNotification("WARNING: " + text).className = "warn";
    }

    /**
     * A error for the user
     * @param {String} text - Content of the error
     */
    static error(text){
        DD.showNotification("ERROR: " + text).className = "error";
    }

    /**
     * A notification for the user
     * @param {String} text - content of the notification
     */
    static notify(text){
        DD.showNotification("INFO: " + text).className = "notify";
    }

    /**
     * Show notification to the front end
     * @param {String} text - Content of the notification
     * @returns {HTMLDivElement}
     */
    static showNotification(text){
        let div = document.createElement('div');
        div.innerHTML = text;
        div.addEventListener('click', function () {
            div.remove();
        });
        setTimeout(function () {
            div.remove();
        }, 12000);
        $('#notifications').appendChild(div);
        return div;
    }
}
