package com.dendrowen.japi;

import com.yourcompany.app.route.WebRoutes;

public class Main {

    /**
     * Start of the application
     * @param args The arguments from the command line
     */
    public static void main(String[] args) {
        String port = Env.get("APPLICATION_PORT");
        if(args.length > 0){
            for(int i = 0; i < args.length; i++) {
                if(args[i].startsWith("-")) {
                    switch (args[i]) {
                        case "-h":
                            App.log("-h          this help file");
                            App.log("-p <port>   open webapp on this port. defaults to %s", Env.get("APPLICATION_PORT"));
                            System.exit(0);
                            break;
                        case "-p":
                            port = args[i + 1];
                            break;
                        default:
                            App.err("unknown parameter '%s'. use -h for help\n", args[i]);
                            System.exit(-1);
                    }
                }
            }
        }

        App app = new App(WebRoutes.class, port);
        app.start();
    }

}
