package com.dendrowen.japi.base;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * A resource controller for a model. It provides an interface for clients on which they can request and provide
 * data to manipulate T
 * @param <T>
 */
public abstract class ResourceController<T> extends Controller {

    private Class<T> resource;

    /**
     * Default constructor
     */
    public ResourceController(){
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        resource = (Class) pt.getActualTypeArguments()[0];
    }

    /**
     * Commonly used to return all occurrences of T
     * @return A list of objects T
     */
    public abstract List<T> index();

    /**
     * Commonly used to create an entry in the database for object T
     * @param obj T
     * @return The created object
     */
    public abstract T create(T obj);

    /**
     * Commonly used to update object T in the database
     * @param obj T
     * @return The updated object
     */
    public abstract T update(T obj);

    /**
     * Commonly used to show object T
     * @param obj T
     * @return Object T
     */
    public abstract T show(T obj);

    /**
     * Commonly used to delete object T from the database
     * @param obj T
     * @return true if the delete has succeeded
     */
    public abstract boolean delete(T obj);

    /**
     * private facade function for create
     * @param obj T
     * @return Object T
     */
    private T _create(Object obj) {
        return create(resource.cast(obj));
    }

    /**
     * private facade function for update
     * @param obj T
     * @return Object T
     */
    private T _update(Object obj) {
        return update(resource.cast(obj));
    }

    /**
     * private facade function for show
     * @param obj T
     * @return Object T
     */
    private T _show(Object obj){
        return show(resource.cast(obj));
    }

    /**
     * private facade function for delete
     * @param obj T
     * @return Object T
     */
    private boolean _delete(Object obj){
        return delete(resource.cast(obj));
    }

    /**
     * return the resource type this class handles
     * @return T
     */

    public Class<T> getResource(){
        return resource;
    }

}
