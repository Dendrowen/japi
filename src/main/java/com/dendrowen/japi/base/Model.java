package com.dendrowen.japi.base;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Base model for this project. For stable use, extend this class when creating a database table representation in Java.
 * @param <T> The class that extends the Model
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@MappedSuperclass
public abstract class Model<T extends Model<T>> {

    protected int id;

    protected Date created_at;

    protected Date updated_at;

    /**
     * Returns the unique record ID
     * @return The id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    /**
     * Get the time at which the object has been created for the first time
     * @return A Date
     */
    @CreationTimestamp
    public Date getCreated_at() {
        return created_at;
    }

    /**
     * Set the created_at value
     * @param created_at A date
     */
    private void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    /**
     * Get the time at which the object was updated last
     * @return A Date
     */
    @UpdateTimestamp
    public Date getUpdated_at() {
        return updated_at;
    }

    /**
     * Set the updated_at value
     * @param updated_at A date
     */
    private void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }
}
