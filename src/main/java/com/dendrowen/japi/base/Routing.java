package com.dendrowen.japi.base;

import com.dendrowen.japi.App;
import com.dendrowen.japi.Auth;
import com.dendrowen.japi.DB;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;

/**
 * Responsible for binding the API requests to the ResourceControllers. Extend it and provide it with an App constructor.
 */
public abstract class Routing {

    private boolean handled;
    private boolean sawResourceHandler = false;
    private String result = "";
    private int statusCode = 200;
    private Map<String, String> body;

    /**
     * Define all you custom routes in {@link #routes()}. Resource routes are generated automatically if a
     * ResourceController exists for the resource. If a route is duplicate. the custom route will execute.
     * -------------------------------------------------------------------------------------------------------------
     * Default constructor. Gets created every request
     */
    public Routing(){
        this.handled = false;
    }

    /**
     * evaluates the callback on a GET request on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @throws Exception When something unexpected happens
     */
    protected void get(String uri, Function<Map<String, String>, String> callback) throws Exception {
        match(uri, callback, "get");
    }

    /**
     * evaluates the callback on a POST request on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @throws Exception When something unexpected happens
     */
    protected void post(String uri, Function<Map<String, String>, String> callback) throws Exception {
        match(uri, callback, "post");
    }

    /**
     * evaluates the callback on a PATCH request on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @throws Exception When something unexpected happens
     */
    protected void patch(String uri, Function<Map<String, String>, String> callback) throws Exception {
        match(uri, callback, "patch");
    }

    /**
     * evaluates the callback on a DELETE request on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @throws Exception When something unexpected happens
     */
    protected void delete(String uri, Function<Map<String, String>, String> callback) throws Exception {
        match(uri, callback, "delete");
    }

    /**
     * evaluates the callback on any request method on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @throws Exception When something unexpected happens
     */
    protected void any(String uri, Function<Map<String, String>, String> callback) throws Exception {
        match(uri, callback, "get", "post", "patch", "delete");
    }

    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Edit the provided object with data from the request body
     * @param obj The object to edit
     * @return The edited object
     */
    private Object editFromBody(Object obj) {
        if(obj == null){
            throw new NullPointerException("parameter obj cannot by null");
        }
        for(Map.Entry<String, String> entry : getBody().entrySet()){
            try{
                Field f = obj.getClass().getDeclaredField(entry.getKey());
                boolean accessible = f.canAccess(obj);
                f.setAccessible(true);
                if (int.class.equals(f.getType())) {
                    f.set(obj, Integer.parseInt(entry.getValue()));
                } else if (String.class.equals(f.getType())) {
                    f.set(obj, entry.getValue());
                } else {
                    int id = Integer.parseInt(entry.getValue());
                    Object rel = DB.get((Class<? extends Model>) f.getType(), id);
                    f.set(obj, rel);
                }
                f.setAccessible(accessible);
            } catch (NoSuchFieldException e) {
                System.err.printf("invalid field '%s' for '%s'. Skipping entry.\n", entry.getKey(), obj.getClass().getName());
            } catch (IllegalAccessException e) {
                System.err.printf("Field '%s' from Class '%s' could not be accessed!\n", entry.getKey(), obj.getClass().getName());
                e.printStackTrace();
            }
        }
        return obj;
    }

    /**
     * Make an object from the data in the request body in the form of the provided resource
     * @param resource The class type to instantiate the object
     * @return The created object
     */
    private Object makeFromBody(Class<?> resource) {
        try {
            Constructor<?> cons = resource.getConstructor();
            Object obj = null;
            try {
                obj = cons.newInstance();
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
                App.err("%s could not be instantiated!\n", resource.getName());
                e.printStackTrace();
            }
            return editFromBody(obj);
        } catch (NoSuchMethodException e) {
            App.err("%s doesn't have an empty constructor!\n", resource.getName());
            e.printStackTrace();
        }
        return new Object();
    }

    /**
     * resource an entire resourceController and handle object mutation
     * @param resourceControllerClass The extension of resourceController class
     */
    public void resource(Class<? extends ResourceController<?>> resourceControllerClass) {
        sawResourceHandler = true;
        if (handled) return;
        List<String> uri = getUriAsList();

        try {
            ResourceController<?> rc = resourceControllerClass.getConstructor().newInstance();
            Class<?> resource = rc.getResource();
            if (!matchResourceUri(resource.getSimpleName() + "s")) return;

            handled = true;

            ObjectMapper mapper = new ObjectMapper();

            if(matchMethods("get")){
                if(!Auth.canRead()){
                    statusCode = 401;
                } else if(uri.size() == 1) {
                    try {
                        result = mapper.writeValueAsString(rc.index());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    int id = Integer.parseInt(uri.get(1));
                    Object obj = DB.get(resource, id);
                    if(uri.size() == 2) {
                        result = callMethod("_show", obj, rc);
                    } else {
                        Field f = obj.getClass().getDeclaredField(uri.get(2));
                        boolean accessible = f.canAccess(obj);
                        f.setAccessible(true);
                        List<?> list = (List<?>) f.get(obj);
                        f.setAccessible(accessible);
                        result = mapper.writeValueAsString(list);
                    }
                }
            } else if (matchMethods("post")){
                if(!Auth.canCreate()){
                    statusCode = 401;
                } else {
                    statusCode = 201;
                    Object obj = makeFromBody(rc.getResource());
                    result = callMethod("_create", obj, rc);
                }
            } else if (matchMethods("patch")){
                if(!Auth.canUpdate()){
                    statusCode = 401;
                } else {
                    statusCode = 201;
                    int id = Integer.parseInt(fromBody("id"));
                    if (id > 0) {
                        Object obj = DB.get(resource, id);
                        editFromBody(obj);
                        result = callMethod("_update", obj, rc);
                    }
                }
            } else if (matchMethods("delete")){
                if(!Auth.canDelete()){
                    statusCode = 401;
                } else {
                    int id = Integer.parseInt(uri.get(1));
                    Object obj = DB.get(resource, id);
                    if (obj != null && callMethod("_delete", obj, rc).equals("1")) {
                        statusCode = 204;
                    } else {
                        statusCode = 409;
                    }
                }
            }
            // TODO: check if necessary
//            else {
//                App.sendResponse(App.getExchange(), 403, App.message("invalid message"));
//            }
        } catch (Exception e) {
            App.sendResponse(App.getExchange(), 500, App.message("something went wrong"));
            e.printStackTrace();
        }
    }

    /**
     * Get a single value from the request body
     * @param key The key
     * @return The value if found. Otherwise null
     */
    public String fromBody(String key){
        return getBody().getOrDefault(key, null);
    }

    /**
     * Call a method from the provided resource controller
     * @param name The name of the method
     * @param param The parameter to be passed to that method
     * @param rc The resourceController object
     * @return A json String representation of the return value
     */
    private String callMethod(String name, Object param, ResourceController<?> rc)  {
        try {
            Method m = rc.getClass().getSuperclass().getDeclaredMethod(name, Object.class);
            m.setAccessible(true);
            Object o = m.invoke(rc, param);
            if (o instanceof Boolean) {
                o = (Boolean) o ? 1 : 0;
            } else {
                o = rc.getResource().cast(o);
            }
            m.setAccessible(false);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(o);
        } catch (IOException e) {
            App.err("Could not write object of type `%s` to json\n", rc.getResource().getName());
            e.printStackTrace();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            App.err("Could not call method `%s` from Class `%s`\n", name, rc.getClass().getName());
            e.printStackTrace();
        }
        return "[]";
    }

    /**
     * evaluates the callback on the provided requests on the provided uri
     * @param uri The uri to match following /api/
     * @param callback The method to execute when it matches
     * @param methods The methods to match
     * @throws Exception When something unexpected happens
     */
    protected void match(String uri, Function<Map<String, String>, String> callback, String... methods) throws Exception {
        if(sawResourceHandler) throw new Exception("Resource handler before normal handler in routes");
        if(handled) return;                 // once a route is handled, don't handle another one
        if(!matchMethods(methods)) return;  // check if the request method matches
        if(!matchUri(uri)) return;          // check if the URI matches
        Map<String, String> params = stripParams(uri);
        appendBody(params);
        try {
            result = callback.apply(params);    // execute the method
        } catch (Exception e) {
            statusCode = 403;
            result = App.message("invalid user");
//            App.sendResponse(App.getExchange(), 403, App.message("invalid user"));
        }
        handled = true;
    }

    /**
     * Append the body parameters to params
     * @param params A map to append parameters from body to
     */
    private void appendBody(Map<String, String> params) {
        getBody().forEach(
                (key, value) -> params.merge(key, value, (v1, v2) -> v2)
        );
    }

    /**
     * Get all body parameters
     * @return A Map containing all the body parameters
     */
    private Map<String, String> getBody() {
        if(body == null || body.isEmpty()) {
            InputStream in = App.getExchange().getRequestBody();
            body = new LinkedHashMap<>();
            try {
                byte[] bytes = in.readAllBytes();
                String input = new String(bytes);
                if (input.length() > 0) {
                    JSONObject obj = new JSONObject(input);
                    for (Iterator<String> it = obj.keys(); it.hasNext(); ) {
                        String key = it.next();
                        body.put(key, obj.optString(key, String.valueOf(obj.optInt(key, 0))));
                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        return body;
    }

    /**
     * Get all the uri parameters like category/{cat_id}/products
     * @param uri The provided uri
     * @return A map containing key/value pairs
     */
    private Map<String, String> stripParams(String uri) {
        // split URI's into lists
        List<String> actual = getUriAsList();
        List<String> query = getUriAsList(uri);

        Map<String, String> params = new LinkedHashMap<>();

        for (int i = 0; i < Math.min(actual.size(), query.size()); i++) {
            if (query.get(i).startsWith("{")) {
                String key = query.get(i).replace("{", "").replace("}", "");
                params.put(key, actual.get(i));
            }
        }

        if (App.getExchange().getRequestURI().getQuery() != null) {
            String[] qry = App.getExchange().getRequestURI().getQuery().split("&");
            for (String q : qry) {
                params.put(q.split("=")[0], q.split("=")[1]);
            }
        }

        return params;
    }

    /**
     * Check if the actual method is in methods
     * @param methods The methods to check with
     * @return true if there is a match
     */
    private boolean matchMethods(String... methods){
        boolean methodMatch = false;
        for(String method : methods) {
            if (App.getExchange().getRequestMethod().equalsIgnoreCase(method)){
                methodMatch = true;
            }
        }
        return methodMatch;
    }

    /**
     * Match the parameterized uri to the actual uri. products/{id} will match products/3
     * @param uri The parameterized uri
     * @return true if there is a match
     */
    private boolean matchUri(String uri){
        // split URI's into lists
        List<String> actual = getUriAsList();
        List<String> query = getUriAsList(uri);

        // size of the query and actual should be equal
        if(query.size() != actual.size()) return false;

        for(int i = 0; i < query.size(); i++){
            if(query.get(i).startsWith("{")) continue; // skip parameterized queries
            if(!query.get(i).equalsIgnoreCase(actual.get(i))) return false;
        }

        return true;
    }

    /**
     * Checks if a resource is addressed
     * @param uri The provided resource name in plural
     * @return true if it matches
     */
    protected boolean matchResourceUri(String uri){
        // split URI's into lists
        List<String> actual = getUriAsList();
        List<String> query = getUriAsList(uri);

        return actual.get(0).equalsIgnoreCase(query.get(0));
    }

    /**
     * Get the uri following the BASE_API_URL in App
     * @return A List split of the uri split on '/'
     */
    public static List<String> getUriAsList(){
        List<String> uri = getUriAsList(App.getExchange().getRequestURI().getPath());

        // remove leading uri parameters from the actual uri
        while(!uri.get(0).equalsIgnoreCase(App.BASE_API_URL)){
            uri.remove(0);
        }
        uri.remove(0);
        return uri;
    }

    /**
     * Splits a given uri into a list
     * @param uri the uri to split
     * @return the uri split on '/'
     */
    private static List<String> getUriAsList(String uri){
        List<String> splitUri = new LinkedList<>(Arrays.asList(uri.split("/")));
        return splitUri;
    }

    /**
     * Return the result. It is generated by the resource and customResource methods
     * @return The result as a json String
     */
    public String getResult() {
        return result;
    }

    /**
     * Checks whether the request has been handled. It does not always mean the 'result' is set
     * @return true if the call has been handled
     */
    public boolean isHandled() {
        return handled;
    }

    /**
     * Define all you custom routes here. Resource routes are generated automatically if a ResourceController exists
     * for the resource. If a route is duplicate. the custom route will execute.
     */
    public abstract void routes() throws Exception;
}
