package com.dendrowen.japi.base;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public abstract class Controller {

    /**
     * Convert an object or list to json
     * @param object the object or list to convert
     * @return A JSON string representation of the provided object
     * @throws IOException if the conversion fails
     */
    protected static String toJson(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

}
