package com.dendrowen.japi;


import com.dendrowen.japi.base.ResourceController;
import com.dendrowen.japi.base.Routing;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.reflections.Reflections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;

/**
 * The base of this framework. Create one in your main and start it
 */
public class App extends Thread {

    public static final int DEFAULT_PORT = 8000;
    public static final String BASE_API_URL = "api";

    private HttpServer server = null;
    private String port;
    private Class<? extends Routing> routeClass;

    private static Map<Long, HttpExchange> exchangeMap = new HashMap<>();

    /**
     * Base constructor for the app
     * @param routeClass An extension of Routing
     */
    public App(Class<? extends Routing> routeClass, String port) {
        this.routeClass = routeClass;
        this.port = port;
    }

    public static void log(String msg){
        System.out.printf("%3d | %s\n", Thread.currentThread().getId(), msg);
    }

    public static void log(String msg, Object... args){
        log(String.format(msg, args));
    }

    public static void err(String msg){
        System.err.printf("%3d | %s\n", Thread.currentThread().getId(), msg);
    }

    public static void err(String msg, Object... args){
        err(String.format(msg, args));
    }

    /**
     * Deploy the server
     */
    @Override
    public void run(){
        if(server != null) return;
        try {
            server = HttpServer.create(new InetSocketAddress(Integer.parseInt(port)), 0);

            server.createContext("/" + Env.get("API_URL_SUFFIX"),
                    backend());

            server.createContext("/",
                    frontend());

            server.setExecutor(Executors.newCachedThreadPool());

            server.start();

            log("SERVER RUNNING ON PORT %s", port);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Initiate the frontend server
     * @return The instance
     */
    private HttpHandler frontend(){
        return exchange -> {
            log("FILE: %6s: %s%s", exchange.getRequestMethod(), exchange.getRequestHeaders().getFirst("Host"), exchange.getRequestURI());
            try {
                String filename = exchange.getRequestURI().getPath();
                byte[] bytes;
                if (filename.length() <= 1) {
                    filename = "/index.html";
                }

                bytes = readBytesFromFile(filename);

                if(filename.endsWith(".html")){
                    String content = new String(bytes);
                    File folder = new File(getClass().getClassLoader().getResource("public/js").toURI());
                    File[] listOfFiles = folder.listFiles();
                    for (int i = 0; i < listOfFiles.length; i++) {
                        content = adddJsFiles(content, "js/" + listOfFiles[i].getName());
                    }
                    bytes = content.getBytes();
                }


                sendResponse(exchange, 200, bytes);
            } catch (FileNotFoundException e) {
                err("WARNING: could not find '" + exchange.getRequestURI() + "'");
                exchange.getResponseHeaders().add("Content-Type", "application/json");
                sendResponse(exchange, 404, message("page not found"));
            } catch (Exception e){
                e.printStackTrace();
            }
        };
    }

    /**
     * Add a js file to the HTML
     * @param html The HTML that needs expanding
     * @param js The js filename
     * @return A new html string
     */
    private String adddJsFiles(String html, String js){
        return html.replace("</body>", "<script src=\"" + js + "\"></script>\n</body>");
    }

    /**
     * Read all the bytes from a file
     * @param filename Filename in the resources
     * @return the file as bytes
     * @throws IOException
     */
    private byte[] readBytesFromFile(String filename) throws IOException {
        InputStream file = getClass().getClassLoader().getResourceAsStream("public" + filename);
        if(file != null) {
            return file.readAllBytes();
        }
        return new byte[0];
    }

    /**
     * Initiate the backend server
     * @return The instance
     */
    private HttpHandler backend(){
        return exchange -> {
            log("<= NEW TRANSACTION");
            log("%6s: %s%s", exchange.getRequestMethod(), exchange.getRequestHeaders().getFirst("Host"), exchange.getRequestURI());
            // Save exchange for access from other locations
            exchangeMap.put(Thread.currentThread().getId(), exchange);

            try {
                Routing route = routeClass.getDeclaredConstructor().newInstance();
                exchange.getResponseHeaders().add("Content-Type", "application/json");
                try {
                    route.routes();
                    if(!route.isHandled()) {
                        Reflections reflections = new Reflections(Env.get("PACKAGE_CONTROLLERS"));
                        Set<Class<? extends ResourceController>> resourceClasses = reflections.getSubTypesOf(ResourceController.class);
                        for (Class<?> resourceClass : resourceClasses) {
                            route.resource((Class<? extends ResourceController<?>>) resourceClass);
                        }
                    }
                    if(Auth.getUser() != null) Auth.refreshToken(Auth.getUser());
                } catch (Exception e){
                    e.printStackTrace();
                    sendResponse(exchange, 500, message("could not process data"));
                }
                if(route.isHandled()) {
                    sendResponse(exchange, route.getStatusCode(), route.getResult());
                } else {
                    err("WARNING: could not find'" + exchange.getRequestURI() + "'");
                    sendResponse(exchange, 404, message("data not found"));
                }
            } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
            exchangeMap.remove(Thread.currentThread().getId());
            log("<= TRANSACTION ENDED");
        };
    }

    /**
     * Get the exchange of the current request. It is identified by thread ID
     * @return The current HttpExchange
     */
    public static HttpExchange getExchange(){
        return exchangeMap.get(Thread.currentThread().getId());
    }

    /**
     * Send a custom response to the client
     * @param exchange The http object
     * @param status the http status code
     * @param body the body to be sent back in bytes
     */
    public static void sendResponse(HttpExchange exchange, int status, byte[] body){
        try {
            exchange.sendResponseHeaders(status, body.length == 0 ? -1 : body.length);
            if(body.length > 0){
                exchange.getResponseBody().write(body);
                exchange.getResponseBody().flush();
            }
            exchange.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Send a custom response to the client
     * @param exchange The http object
     * @param status the http status code
     * @param body the body to be sent back as a String
     */
    public static void sendResponse(HttpExchange exchange, int status, String body){
        sendResponse(exchange, status, body.getBytes());
    }

    /**
     * Create a json message
     * @param message the message
     * @return a json Sting
     */
    public static String message(String message){
        return "{\"message\":\"" + message + "\"}";
    }
}