package com.dendrowen.japi;

import com.dendrowen.japi.base.Routing;
import com.yourcompany.app.model.Role;
import com.yourcompany.app.model.User;
import org.hibernate.Session;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.Query;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

public class Auth {

    private static final SecureRandom RAND = new SecureRandom();
    private static final int ITERATIONS = 65536;
    private static final int KEY_LENGTH = 512;
    private static final String ALGORITHM = "PBKDF2WithHmacSHA512";
    public static final Pattern userRegex = Pattern.compile("^[a-zA-Z0-9_-]{3,16}$");
    public static final Pattern passRegex = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d\\W]{8,}$");

    /**
     * Create a has from a password using the algorithm in Auth.ALGORITHM and the salt provided
     * @param password The password to be hashed
     * @param salt The salt to include in the hash
     * @return The hashed password
     */
    public static Optional<String> hashPassword(String password, String salt){
        char[] chars = password.toCharArray();
        byte[] bytes = salt.getBytes();

        PBEKeySpec spec = new PBEKeySpec(chars, bytes, ITERATIONS, KEY_LENGTH);

        try{
            SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
            byte[] securePassword = factory.generateSecret(spec).getEncoded();
            return Optional.of(Base64.getEncoder().encodeToString(securePassword));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Generate a random string
     * @param length The number of characters for the string
     * @return The generated string
     */
    public static Optional<String> generateSalt(int length){
        if (length < 1) {
            System.err.println("length should be > 0");
            return Optional.empty();
        }

        byte[] salt = new byte[length];
        RAND.nextBytes(salt);
        return Optional.of(Base64.getEncoder().encodeToString(salt));
    }

    /**
     * Verify the password with the provided salt against the key
     * @param password The un-hashed password
     * @param key The hashed password from DB
     * @param salt The salt from DB
     * @return true if the password is correct
     */
    public static boolean verifyPassword (String password, String key, String salt) {
        Optional<String> optEncrypted = hashPassword(password, salt);
        return optEncrypted.map(s -> s.equals(key)).orElse(false);
    }

    /**
     * Can the logged in user create the current entity
     * @return true id the role is permitted
     */
    public static boolean canCreate(){
        return can(Role.CREATE_STRING, getRole());
    }

    /**
     * Can the logged in user read the current entity
     * @return true id the role is permitted
     */
    public static boolean canRead(){
        return can(Role.READ_STRING, getRole());
    }

    /**
     * Can the logged in user update the current entity
     * @return true id the role is permitted
     */
    public static boolean canUpdate(){
        return can(Role.UPDATE_STRING, getRole());
    }

    /**
     * Can the logged in user delete the current entity
     * @return true id the role is permitted
     */
    public static boolean canDelete(){
        return can(Role.DELETE_STRING, getRole());
    }

    /**
     * Can a certain role perform a certain operation. The entity on which to perform the operation is retrieved from the url
     * @param op The operation to perform
     * @param role The role that needs the permission
     * @return true is the role is permitted
     */
    private static boolean can(String op, Role role){
        List<String> uri = Routing.getUriAsList();
        String resource = uri.get(uri.size() - (uri.size() % 2 == 0 ? 2 : 1));
        Map<String, List<String>> pMap = role.getPrivilegesMapped();
        List<String> privileges = pMap.get(resource);
        if(privileges == null) return false;
        return privileges.contains(op);
    }

    /**
     * Get the role of the currently logged in user
     * @return The role of the currently logged in user
     */
    private static Role getRole(){
        if(getUser() != null) return getUser().getRole();

        Session session = DB.createSession();
        String query = "FROM Role WHERE name = :name";
        Query q = session.createQuery(query);
        q.setParameter("name", "everyone");
        if(q.getResultList().size() > 0) {
            return (Role) q.getResultList().get(0);
        }

        Role role = new Role();
        role.setName("everyone");
        DB.persist(role);
        return role;
    }

    /**
     * Get the currently logged in user
     * @return The logged in user
     */
    public static User getUser(){
        String token = App.getExchange().getRequestHeaders().getFirst("auth");

        if(token == null) return null;

        Session session = DB.createSession();
        String query = "FROM User WHERE token = :token";
        Query q = session.createQuery(query);
        q.setParameter("token", token);

        if(q.getResultList().size() == 0) return null;

        return (User) q.getResultList().get(0);
    }

    /**
     * Refresh the user token and add the new token to the request
     * @param user The user of which the token should be refreshed
     */
    public static void refreshToken(User user){
        user.setToken(Auth.generateSalt(64).orElse(""));
        DB.update(user);
        App.getExchange().getResponseHeaders().set("auth", user.getToken());
    }

    /**
     * Destroy the current session
     */
    public static void logout(){
        try {
            User user = Auth.getUser();
            assert user != null;
            user.setToken("");
            DB.update(user);
        } catch (NullPointerException e){
            App.err("Tried to log out non-existent user");
        }
    }

    /**
     * Retrieve the user is  the password is correct
     * @param username The username
     * @param password The un-hashed password
     * @return The user corresponding with the given username if the password is correct. Null otherwise
     */
    public static User login(String username, String password){
        Session session = DB.createSession();
        String query = "FROM User WHERE username = :username";
        Query q = session.createQuery(query);
        q.setParameter("username", username);
        User user = (User) q.getResultList().get(0);
        if(Auth.verifyPassword(password, user.getPassword(), user.getSalt())) {
            refreshToken(user);
            return user;
        }
        return null;
    }

}
