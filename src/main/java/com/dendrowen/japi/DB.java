package com.dendrowen.japi;

import com.dendrowen.japi.base.Model;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.reflections.Reflections;

import javax.persistence.Query;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * Facade for the Session from Hibernate
 */
public class DB {

    private SessionFactory sessionFactory;

    /**
     * Private constructor for singleton
     */
    private DB() {
        buildSessionFactory();
    }

    /**
     * Get an object from the database based on its id
     * @param resource The represented model
     * @param id The database id
     * @return The object of class resource if a record is found. null otherwise
     */
    public static Object getObject(Class<? extends Model> resource, int id) {
        Session s = createSession();
        s.beginTransaction();
        Object res = s.get(resource.getDeclaringClass(), id);
        s.close();
        return res;
    }

    /**
     * create a factory to create sessions for the database connection
     */
    private void buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();

            Properties properties = new Properties();

            properties.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
            properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
            properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "org.hibernate.context.internal.ThreadLocalSessionContext");
            properties.put(Environment.URL,  String.format("jdbc:mysql://%s:%s/%s", Env.get("DB_ADDRESS"), Env.get("DB_PORT"), Env.get("DB_DATABASE")));
            properties.put(Environment.USER, Env.get("DB_USERNAME"));
            properties.put(Environment.PASS, Env.getOrElse("DB_PASSWORD", ""));

            configuration.setProperties(properties);

            Reflections reflections = new Reflections(Env.get("PACKAGE_MODELS"));
            Set<Class<? extends Model>> models = reflections.getSubTypesOf(Model.class);
            for(Class<?> model : models){
                configuration.addAnnotatedClass(model);
            }

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Roll back the queries performed since creating the session of committing it.
     * @param session The session of which the DB should rollback
     */
    public static void rollBack(Session session){
        if(session != null && session.getTransaction() != null){
            System.out.println("DB: Rolling back");
            session.getTransaction().rollback();
        }
    }

    /**
     * Close an active session
     * @param session the session to be closed
     */
    public static void close(Session session){
        if(session != null && session.isOpen()){
            session.close();
        }
    }

    /**
     * Create a new session
     * @return a new session
     */
    public static Session createSession(){
        return getInstance().sessionFactory.openSession();
    }

    private static DB instance;

    /**
     * Get the DB object if present. Otherwise create one
     * @return The DB instance
     */
    public static synchronized DB getInstance() {
        if (instance == null) {
            instance = new DB();
        }
        return instance;
    }

    /**
     * Get an object from the database based on its id
     * @param res The represented Class
     * @param id The database id
     * @param <T> The class extending Model
     * @return The database record if found. null otherwise
     */
    public static <T> T get(Class<T> res, int id) {
        Session s = createSession();
        s.beginTransaction();
        Object obj = s.get(res, id);
        s.close();
        return (T) obj;
    }

    /**
     * Get all records from a database table
     * @param res The Model representation of a table
     * @param <T> The class extending model
     * @return A list of objects
     */
    public static <T> List<T> getAll(Class<T> res){
        Session s = createSession();
        s.beginTransaction();
        Query q = s.createQuery("from " + res.getSimpleName(), res);
        List<T> rs = q.getResultList();
        close(s);
        return rs;
    }

    /**
     * Save an object and it's children to the database
     * @param obj the object to be saved
     * @return true is the saving was successful
     */
    public static boolean persist(Object obj){
        Session s = createSession();
        boolean success = false;
        try {
            s.beginTransaction();
            s.persist(obj);
            s.getTransaction().commit();
            success = true;
        } catch (Exception e){
            rollBack(s);
            e.printStackTrace();
        } finally {
            close(s);
        }
        return success;
    }

    /**
     * Update an object and it's children to the database
     * @param obj the object to be updated
     * @return true if it was a success
     */
    public static boolean update(Object obj) {
        Session s = createSession();
        boolean success = false;
        try{
            s.beginTransaction();
            s.update(obj);
            s.getTransaction().commit();
            success = true;
        } catch (Exception e) {
            rollBack(s);
            e.printStackTrace();
        } finally {
            close(s);
        }
        return success;
    }

    /**
     * Delete an object from the database
     * @param obj the object to be deleted
     * @return true is it was a success
     */
    public static boolean delete(Object obj) {
        Session s = createSession();
        boolean success = false;
        try {
            s.beginTransaction();
            s.delete(obj);
            s.getTransaction().commit();
            success = true;
        } catch (Exception e){
            e.printStackTrace();
            rollBack(s);
        } finally {
            close(s);
        }
        return success;
    }
}
