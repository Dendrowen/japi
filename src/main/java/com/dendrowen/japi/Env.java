package com.dendrowen.japi;

import io.github.cdimascio.dotenv.Dotenv;

public class Env {

    private static final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    /**
     * Get an environment variable
     * @param key The variable name
     * @return The value
     */
    public static String get(String key){
        return dotenv.get(key);
    }

    /**
     * Get an environment variable
     * @param key The variable name
     * @param def The fallback value
     * @return The value if 'key' exists. def otherwise
     */
    public static String getOrElse(String key, String def){
        String v = dotenv.get(key);
        if(v == null){
            return def;
        }
        return v;
    }

}
