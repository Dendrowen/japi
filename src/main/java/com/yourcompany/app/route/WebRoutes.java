package com.yourcompany.app.route;

import com.yourcompany.app.controller.LoginController;
import com.dendrowen.japi.base.Routing;

public class WebRoutes extends Routing {

    /**
     * Define all you custom routes in {@link #routes()}. Resource routes are generated automatically if a
     * ResourceController exists for the resource. If a route is duplicate. the custom route will execute.
     * -------------------------------------------------------------------------------------------------------------
     * Default constructor. Gets created every request
     */
    public WebRoutes() {
        super();
    }

    @Override
    public void routes() throws Exception {
        get("sessions", LoginController::loggedIn);
        post("sessions", LoginController::login);
        delete("sessions", LoginController::logout);
    }
}
