package com.yourcompany.app.model;

import com.dendrowen.japi.base.Model;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.*;

@Entity
@Table(name = "roles")
public class Role extends Model<Role> {

    public final static int CREATE = 0b1000;
    public final static int READ   = 0b0100;
    public final static int UPDATE = 0b0010;
    public final static int DELETE = 0b0001;
    public static final String CREATE_STRING = "create";
    public static final String READ_STRING = "read";
    public static final String UPDATE_STRING = "update";
    public static final String DELETE_STRING = "delete";

    private String name;
    private String privileges;

    private Map<String, Integer> permissions;

    public Role(){
        setPrivileges("{}");
    }

    @JsonIgnore
    public String getPrivileges() {
        return privileges;
    }

    private void setPrivileges(String privileges) {
        this.privileges = privileges;
        permissions = new HashMap<>();
        if (privileges.length() > 0) {
            JSONObject obj = new JSONObject(privileges);
            for (Iterator<String> it = obj.keys(); it.hasNext(); ) {
                String key = it.next();
                permissions.put(key, obj.optInt(key, 0));
            }
        }
    }

    @JsonIgnore
    @Transient
    public boolean hasPermission(String key, int type){
        return (permissions.getOrDefault(key, 0) & type) > 0;
    }

    @JsonIgnore
    @Transient
    public void setPermission(String key, int type, boolean enable){
        int p = permissions.getOrDefault(key, 0);
        p = enable ? ( p | type ) : ( p & ~type );
        permissions.put(key, p);
        storePrivileges();
    }

    @JsonIgnore
    @Transient
    private void storePrivileges(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            privileges = mapper.writeValueAsString(permissions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Transient
    public Map<String, List<String>> getPrivilegesMapped(){
        Map<String, List<String>> pMap = new HashMap<>();
        for(String key : permissions.keySet()){
            List<String> crud = new ArrayList<>();
            if(hasPermission(key, CREATE)) crud.add(CREATE_STRING);
            if(hasPermission(key, READ  )) crud.add(READ_STRING);
            if(hasPermission(key, UPDATE)) crud.add(UPDATE_STRING);
            if(hasPermission(key, DELETE)) crud.add(DELETE_STRING);
            pMap.put(key, crud);
        }
        return pMap;
    }
}
