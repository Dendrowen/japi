package com.yourcompany.app.controller;

import com.dendrowen.japi.DB;
import com.dendrowen.japi.base.ResourceController;
import com.yourcompany.app.model.Role;

import java.util.List;

public class RoleController extends ResourceController<Role> {
    @Override
    public List<Role> index() {
        return DB.getAll(Role.class);
    }

    @Override
    public Role create(Role obj) {
        if(DB.persist(obj)) {
            return obj;
        }
        return null;
    }

    @Override
    public Role update(Role obj) {
        if(DB.update(obj)){
            return obj;
        }
        return null;
    }

    @Override
    public Role show(Role obj) {
        return obj;
    }

    @Override
    public boolean delete(Role obj) {
        return DB.delete(obj);
    }
}
