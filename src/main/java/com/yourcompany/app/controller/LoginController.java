package com.yourcompany.app.controller;

import com.dendrowen.japi.App;
import com.dendrowen.japi.base.Controller;
import com.dendrowen.japi.Auth;
import com.yourcompany.app.model.User;

import java.io.IOException;
import java.util.Map;

public class LoginController extends Controller {

    /**
     * Login method
     * @param params parameters containing at least a username and a password
     * @return The user corresponding with the provided data. An error message otherwise
     */
    public static String login(Map<String, String> params) {
        User user = Auth.login(params.get("username"), params.get("password"));
        if(user != null) {
            try {
                return toJson(user);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return App.message("Could not login");
    }

    /**
     * Check to see if a user is logged in
     * @param params none
     * @return The logged in user or an error message
     */
    public static String loggedIn(Map<String, String> params){
        try {
            if (Auth.getUser() != null) {
                return toJson(Auth.getUser());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return App.message("not logged in");
    }

    /**
     * Logout the currently logged in user
     * @param params none
     * @return A message
     */
    public static String logout(Map<String, String> params){
        Auth.logout();
        return App.message("logged out");
    }
}
