package com.yourcompany.app.controller;

import com.dendrowen.japi.DB;
import com.dendrowen.japi.base.ResourceController;
import com.dendrowen.japi.Auth;
import com.yourcompany.app.model.User;

import java.util.List;
import java.util.regex.Matcher;

public class UserController extends ResourceController<User> {

    @Override
    public List<User> index() {
        return DB.getAll(User.class);
    }

    @Override
    public User create(User obj) {
        Matcher mUser = Auth.userRegex.matcher(obj.getUsername());
        if(!mUser.find()){
            return null;
        }
        Matcher mPass = Auth.passRegex.matcher(obj.getPassword());
        if(!mPass.find()){
            return null;
        }
        obj.setSalt(Auth.generateSalt(64).orElse(""));
        if(obj.getSalt().length() == 0) return null;

        obj.setPassword(Auth.hashPassword(obj.getPassword(), obj.getSalt()).orElse(""));
        if(obj.getPassword().length() == 0) return null;

        DB.persist(obj);
        return obj;
    }

    @Override
    public User update(User obj) {
        return null;
    }

    @Override
    public User show(User obj) {
        return null;
    }

    @Override
    public boolean delete(User obj) {
        return false;
    }
}
